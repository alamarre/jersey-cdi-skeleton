package com.generic.apis;

import com.generic.utilities.GenericUtility;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/Health")
@RequestScoped
public class Health {
    
    @Inject
    GenericUtility genericUtility;

    @GET
    public Response handleGet() {
        return Response.ok(genericUtility.getMessage()).build();
    }
}
