What is this?
-------------
Just a simple Maven project to use as a starting point for building REST APIs with dependency injection using CDI and Jersey.

Getting Started
---------------
Build with Maven. 
Run with Tomcat. 
Go to {{rootUrl}}/api/v1/Health to make sure it's working
Make something great

Running the easy way
--------------------
Install Netbeans Java EE with Tomcat
Install the Netbeans Maven plugin, if it's not in the base package you downloaded
File -> Open Project and select the folder that the project is located in
Right click the project and click run

Licensing
---------
None. This lacks sufficient creativity to be covered by copyright. Use however you want with no restrictions.


